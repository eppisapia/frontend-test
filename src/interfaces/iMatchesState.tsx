export interface iMatches {
    homeTeam: iTeamScore;
    awayTeam: iTeamScore;
    id: number;
    stage: string;
}

interface iTeamScore {
    name: string;
    score: number;
}

export default interface iSquadScreenState {
    matches: iResponseGetMatches
}


export interface iResponseGetMatches {
    status: number;
    data: [iMatches];
    message?: string;
}




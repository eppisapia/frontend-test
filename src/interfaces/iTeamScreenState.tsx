export interface iResponseGetTeams {
    status: number;
    data: [iTeam];
    message?: string;
}


export interface iTeam {
    id: number,
    name: string,
    shortName: string,
    tla: string,
    crestUrl: string,
    website: string,
    clubColors: string
}

export default interface iTeamScreenState {
    data: iResponseGetTeams
}
export interface iSquad {
    id: number;
    name: string;
    shirtNumber?: string;
    nationality: string;
}

export default interface iSquadScreenState {
    squad: iResponseGetSquad
}


export interface iResponseGetSquad {
    status: number;
    data: [iSquad];
    message?: string;
}
import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import TeamScreen from './screens/teams/TeamScreen';
import SquadScreen from './screens/squad/SquadScreen';


function App() {
  return (
    <React.Fragment>
      <Router>
        <Switch>
          <Route exact path="/" component={TeamScreen} />
          <Route exact path="/:name/:id" component={SquadScreen} />
        </Switch>
      </Router>
    </React.Fragment >
  );
}

export default App;

import axios from 'axios';

export const newsApi = axios.create({
    baseURL: "http://127.0.0.1:9000",
    timeout: 60000,
    headers: {
        "Access-Control-Allow-Origin": "*",
    },
});


export const getService = (path: string) => {
    return newsApi.get(path);
}
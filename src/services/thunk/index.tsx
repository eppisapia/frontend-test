import { getTeams, generalResponse, getMatches, getSquad, squadResponse, matchesResponse } from '../actions';
import { getService } from '../axios/api';

export function fetchTeams() {
    return function (dispatch: any) {
        dispatch(getTeams());
        return getService("")
            .then(
            response => response.data,
            error => console.log('Error en la accion GET_TEAMAS:', error),
        )
            .then((data) => {
                dispatch(generalResponse(data));
            },
        );
    };
}

export function fetchMatches(id: string) {
    return function (dispatch: any) {
        dispatch(getMatches(id));
        return getService(`/squad/${id}/matches`)
            .then(
            response => response.data,
            error => console.log('Error en la accion GET_MATCHES:', error),
        )
            .then((data) => {
                dispatch(matchesResponse(data));
            },
        );
    };
}

export function fetchSquad(id: string) {
    return function (dispatch: any) {
        dispatch(getSquad(id));
        return getService(`/squad/${id}`)
            .then(
            response => response.data,
            error => console.log('Error en la accion GET_SQUAD:', error),
        )
            .then((data) => {
                dispatch(squadResponse(data));
            },
        );
    };
}
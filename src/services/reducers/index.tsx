import { GET_TEAMS, GET_SQUAD, GET_MATCHES, RESPONSE, SQUAD_RESPONSE, MATCHES_RESPONSE } from '../actions'

const reducer = (state = {}, action: any) => {
    switch (action.type) {
        case GET_TEAMS:
            return { ...state };
        case GET_SQUAD:
            return { ...state };
        case GET_MATCHES:
            return { ...state };
        case RESPONSE:
            return { ...state, data: action.data }
        case SQUAD_RESPONSE:
            return { ...state, squad: action.squad }
        case MATCHES_RESPONSE:
            return { ...state, matches: action.matches }
        default:
            return state;
    }
};
export default reducer;
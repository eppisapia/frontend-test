export const GET_TEAMS = "GET_TEAMS";
export const GET_SQUAD = "GET_SQUAD";
export const GET_MATCHES = "GET_MATCHES";
export const RESPONSE = "RESPONSE";
export const SQUAD_RESPONSE = "SQUAD_RESPONSE";
export const MATCHES_RESPONSE = "MATCHES_RESPONSE";

export const getTeams = () => ({
    type: GET_TEAMS
});

export const getSquad = (teamId: string) => ({
    type: GET_SQUAD,
    teamId
});

export const getMatches = (teamId: string) => ({
    type: GET_MATCHES,
    teamId
})

export const squadResponse = (json: any) => ({
    type: SQUAD_RESPONSE,
    squad: json
})

export const matchesResponse = (json: any) => ({
    type: MATCHES_RESPONSE,
    matches: json
})

export const generalResponse = (json: any) => ({
    type: RESPONSE,
    data: json
})
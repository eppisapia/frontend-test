import React from 'react';
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Team from './screens/teams/components/Team';
import { iTeam } from './interfaces/iTeamScreenState'

const teamMock: iTeam = {
    id: 0,
    crestUrl: "testUrl",
    name: "testName",
    tla: "testTla",
    shortName: "testShortName",
    clubColors: "testColors",
    website: "testWebsite"
}
configure({ adapter: new Adapter() });

describe('<Team /> with mock', () => {
    const team = shallow(
        <Team id={teamMock.id} crestUrl={teamMock.crestUrl} name={teamMock.name} tla={teamMock.tla} shortName={teamMock.shortName} clubColors={teamMock.clubColors} website={teamMock.website} />
    );

    it("should match the snapshot", () => {
        expect(team).toMatchSnapshot();
    })

    it("should have link to website with text Website", () => {
        expect(team.contains(<a href={teamMock.website}>Website</a>)).toBeTruthy()
    })
});
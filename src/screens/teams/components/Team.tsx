import React from 'react'
import { iTeam } from '../../../interfaces/iTeamScreenState'
import { Card, Button } from 'react-bootstrap';
import '../Team.css';
import { Link } from "react-router-dom";

export default function Team(props: iTeam) {
    return (
        <React.Fragment>
            <div className="teamContainer">
                <Card>
                    <Card.Img variant="top" src={props.crestUrl} className="teamCrest" />
                    <Card.Body>
                        <Card.Title>{props.name}</Card.Title>
                        <Button id="website" className="button">
                            <Link to={`/${props.name}/${props.id}`}>Equipo</Link>
                        </Button>
                        <Button className="button"><a href={props.website}>Website</a></Button>
                    </Card.Body>
                </Card>
            </div>
        </React.Fragment>
    )
}
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux'
import { fetchTeams } from '../../services/thunk'
import iTeamScreenState from '../../interfaces/iTeamScreenState'
import Team from './components/Team'
import { Page } from "framer"

function TeamScreen() {
  const [index, setIndex] = useState<number>(0)
  const dispatch = useDispatch()
  const teams = useSelector((state: iTeamScreenState) => state.data)

  useEffect(() => {
    dispatch(fetchTeams())
  }, [dispatch]);

  return (
    <React.Fragment>
      <h1> Selección Italiana </h1>
      {
        teams ?
          <Page paddingLeft={500} style={{ width: "100%", height: "100%" }} alignment="center" defaultEffect={"coverflow"} currentPage={index} contentHeight="auto" contentWidth="auto" gap={teams.data.length}>
            {teams ?.data.map((item, index: number) => {
              return (
                <div key={"item" + index.toString}>
                  <Team id={item.id} crestUrl={item.crestUrl} name={item.name} tla={item.tla} shortName={item.shortName} clubColors={item.clubColors} website={item.website} />
                </div>
              )
            })}
          </Page>

          : null
      }
    </React.Fragment >
  );
}



export default TeamScreen;
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux'
import { fetchMatches } from '../../../services/thunk'
import iSquadScreenState from '../../../interfaces/iMatchesState'
import { iMatches } from '../../../interfaces/iMatchesState'
import '../Squad.css'

type props = { id: string }
function Matches(props: props) {
    const dispatch = useDispatch()
    const matches = useSelector((state: iSquadScreenState) => state.matches)

    useEffect(() => {
        dispatch(fetchMatches(props.id)
        )

    }, [dispatch]);

    return (
        <React.Fragment>
            <div className="matchesContainer squadScreenComponent">
                <h3> Ultimos juegos </h3>
                {
                    matches ?.data ?.map((item: iMatches, index: number) => {
                        return (
                            <div key={"match" + index.toString + item.id} className="matchRow">
                                <div>{item.homeTeam ?.name}</div>
                                <label> {item.homeTeam ?.score} - {item.awayTeam ?.score}</label>
                                <div>{item.awayTeam ?.name}</div>
                            </div>
                        )
                    })
                }
            </div>
        </React.Fragment>

    )
}


export default Matches;
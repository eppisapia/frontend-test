import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux'
import iSquadScreenState from "../../../interfaces/iSquadState"
import { fetchSquad } from '../../../services/thunk'
import { iSquad } from "../../../interfaces/iSquadState"

type props = { id: string }
function Squad(props: props) {
    const dispatch = useDispatch()
    const squad = useSelector((state: iSquadScreenState) => state.squad)

    useEffect(() => {
        dispatch(fetchSquad(props.id))
    }, [dispatch]);

    return (
        <React.Fragment>
            <div className="squadScreenComponent">
                <h3> Equipo </h3>
                {
                    squad ?.data ?.map((item: iSquad, index: number) => {
                        return (
                            <div key={"squad" + index.toString + item.id} className="playerRow">
                                <label> {item.shirtNumber}</label>
                                <label>{item.name}</label>
                                <label>{item.nationality}</label>
                            </div>
                        )
                    })
           }
            </div>
        </React.Fragment>)
}

export default Squad;
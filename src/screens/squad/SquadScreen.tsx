import React, { useEffect } from 'react';
import { Link, RouteComponentProps } from "react-router-dom";
import Matches from './components/Matches'
import Squad from './components/Squad'

type RouteParams = { id: string, name: string }


function SquadScreen({ match }: RouteComponentProps<RouteParams>) {

    return (
        <React.Fragment>
            <div className="squadScreenBackground">
                <div className="goHome">
                    <Link to="/">Regresar</Link>
                </div>
                <h1> {match.params.name} </h1>
                <div className="squadScreenContainer">
                    <Matches id={match.params.id} />
                    <Squad id={match.params.id} />
                </div>
            </div>
        </React.Fragment>
    )
}

export default SquadScreen;